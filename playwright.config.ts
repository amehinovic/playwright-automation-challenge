import { devices, PlaywrightTestConfig } from "@playwright/test"

const config: PlaywrightTestConfig = {
  forbidOnly: !!process.env.CI,
  retries: 0,
  timeout: 360000,
  expect: {
    timeout: 30000,
  },
  use: {
    trace: "on-first-retry",
    screenshot: "only-on-failure",
    video: "retain-on-failure",
    headless: false,
    actionTimeout: 30000,
    navigationTimeout: 30000,
    baseURL: process.env.URL,
    acceptDownloads: true,
  },
  workers: 1,
  fullyParallel: false,
  projects: [
    {
      name: 'Chrome',
      use: { ...devices['Desktop Chrome'] },
    },
  ],
  reporter: [
    ["list"],
    ["json", { outputFile: "test-results.json" }],
    ["junit", { outputFile: "report.xml" }],
    ["html", { open: "never" }]
  ],
}

export default config

